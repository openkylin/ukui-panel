<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>TaskManager::TaskManagerItem</name>
    <message>
        <source>Add launcher to panel</source>
        <translation>將啟動器添加到面板</translation>
    </message>
    <message>
        <source>Remove launcher from panel</source>
        <translation>從面板中刪除啟動器</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TaskManager::UkuiTaskManager</name>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Keep above</source>
        <translation>保持高於</translation>
    </message>
    <message>
        <source>Unset keep above</source>
        <translation>取消設置</translation>
    </message>
</context>
</TS>
