<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>TaskManager::TaskManagerItem</name>
    <message>
        <source>Remove launcher from panel</source>
        <translation>ངོས་ལེབ་ཀྱི་ནང་ནས་སྒུལ་བྱེད་འཕྲུལ་ཆས་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Add launcher to panel</source>
        <translation>སྒུལ་བྱེད་འཕྲུལ་ཆས་ངོས་ལ་སྦྱོར་བ།</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>TaskManager::UkuiTaskManager</name>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བ་བཅས་སུ་འགྱུར་བར་</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ་ཡིན།</translation>
    </message>
    <message>
        <source>Keep above</source>
        <translation>Keepaove(ལས་མཐོ་བ་རྒྱུན་འཁྱོངས་བྱེད་པ། )</translation>
    </message>
    <message>
        <source>Unset keep above</source>
        <translation>སྒྲིག་གཞི་མེད་པར་བཟོ་བ་བཅས་བྱེད་དགོས།</translation>
    </message>
</context>
</TS>
