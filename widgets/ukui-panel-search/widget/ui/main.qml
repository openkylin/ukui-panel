/*
 * Copyright (C) 2024, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15

import org.ukui.quick.widgets 1.0
import org.ukui.panel.search 1.0
import org.ukui.quick.items 1.0
import org.ukui.quick.platform 1.0

WidgetItem {
    id: root
    property bool isHorizontal: Widget.orientation == Types.Horizontal
    property real ratio: parent.ratio ? parent.ratio : 1

    Layout.fillWidth: !isHorizontal
    Layout.fillHeight: isHorizontal
    Layout.preferredWidth: isHorizontal ? childrenRect.width : width
    Layout.preferredHeight: isHorizontal ? height : childrenRect.height

    DtThemeBackground {
        id: backGround
        anchors.left: parent.left
        anchors.top: parent.top
        // 搜索框margin取8
        anchors.topMargin: root.isHorizontal? 8 * root.ratio : 0
        anchors.leftMargin: root.isHorizontal? 0 : 8 * root.ratio
        width: root.isHorizontal? icon.width + searchText.width + 32 : parent.width - anchors.leftMargin * 2
        height: root.isHorizontal? parent.height - anchors.topMargin * 2 : icon.height + icon.anchors.margins * 2
        radius: root.isHorizontal? height / 2 : width / 2
        backgroundColor: mouseArea.containsPress ? GlobalTheme.kContainClick
                                              : mouseArea.containsMouse ? GlobalTheme.kContainHover
                                                                   : GlobalTheme.kContainGeneralNormal
        useStyleTransparency: false
        border.width: 1
        borderColor: GlobalTheme.kLineNormal
        Icon {
            id: icon
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.margins: 8
            width: root.isHorizontal? height : parent.width - anchors.margins * 2
            height: root.isHorizontal? parent.height - anchors.margins * 2 : width
            source: "kylin-search"
            mode: Icon.AutoHighlight
        }
        DtThemeText {
            id: searchText
            anchors.left: icon.right
            anchors.top: backGround.top
            anchors.leftMargin: 8
            anchors.topMargin: (parent.height - height) / 2
            height: contentHeight
            width: contentWidth
            text: qsTr("Search")
            visible: root.isHorizontal
            textColor: GlobalTheme.kFontPrimaryDisable
        }
    }
    MouseArea {
        id: mouseArea
        anchors.fill: backGround
        hoverEnabled: true

        SearchButton {
            id: searchButton
        }

        onClicked: {
            searchButton.openUkuiSearch();
        }
        Tooltip {
            id: tooltip
            anchors.fill: parent
            mainText: Widget.tooltip
            posFollowCursor: false
            location: {
                switch(Widget.container.position) {
                    case Types.Bottom:
                        return Dialog.BottomEdge;
                    case Types.Left:
                        return Dialog.LeftEdge;
                    case Types.Top:
                        return Dialog.TopEdge;
                    case Types.Right:
                        return Dialog.RightEdge;
                }
            }
            margin: 6
        }
    }

    Widget.actions:[
        Action {
            iconName: "document-page-setup-symbolic"
            text: qsTr("Search settings")
            onTriggered: function () {
                searchButton.openSearchSetting();
            }
        }
    ]
}
