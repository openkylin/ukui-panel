<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>Calendar</name>
    <message>
        <source> notification</source>
        <translation> 條通知</translation>
    </message>
    <message>
        <source> notifications</source>
        <translation> 條通知</translation>
    </message>
    <message>
        <source>Notification center</source>
        <translation>通知中心</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Time and Date Setting</source>
        <translation>時間日期設置</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>設置通知中心</translation>
    </message>
</context>
</TS>
