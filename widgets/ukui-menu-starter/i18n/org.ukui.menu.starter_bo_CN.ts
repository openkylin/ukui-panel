<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>StartMenuButton</name>
    <message>
        <source>Lock Screen</source>
        <translation>བརྙན་ཤེལ་ལ་ཟྭ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation>སྤྱོད་མཁན་བརྗེ་དགོས།</translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation>ཐོ་ཁོངས་ནས་སུབ་དགོས།</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>གཉིད་ཁུག་པ།</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation>གཉིད་ཡེར་བ།</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation>བསྐྱར་དུ་སྒོ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Time Shutdown</source>
        <translation>དུས་གཏན་གནམ་གྲུ་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>Power Off</source>
        <translation>གནམ་གྲུའི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Power Manager</source>
        <translation>གློག་ཁུངས་དོ་དམ།</translation>
    </message>
    <message>
        <source>About This Computer</source>
        <translation>འཕྲུལ་ཆས་འདིའི་སྐོར།</translation>
    </message>
    <message>
        <source>Network Settings</source>
        <translation>དྲ་རྒྱའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>Control Center</source>
        <translation>རྒྱུད་རིམ་ལྡན་པའི་སྒོ་ནས་སྒྲིག་</translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation>མཐའ་སྣེའི་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Switch User or Log Out</source>
        <translation>ཐོ་ཁོངས་བརྗེ་བའམ་ཡང་ན་ཐོ་ཁོངས་ནས་བསུབ་དགོས།</translation>
    </message>
    <message>
        <source>Power Options</source>
        <translation>གློག་ཁུངས་བཅས་ཀྱི་རྣམ་གྲངས་འདེམས་དགོས།</translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation>ཅོག་ཙེའི་ངོས་གསལ་པོར་མངོན་པ།</translation>
    </message>
    <message>
        <source>All Applications</source>
        <translation>ཚང་མ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
</context>
</TS>
