/*
 *
 *  * Copyright (C) 2023, KylinSoft Co., Ltd.
 *  *
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *
 *  * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */

#include "remote-config.h"
#include <config-loader.h>
#include <types.h>
#include <island.h>

RemoteConfig::RemoteConfig(QObject *parent) : RemoteConfigSource(parent)
        , m_mergeIcons(GeneralConfigDefine::MergeIcons::Never)
        , m_panelLocation(GeneralConfigDefine::PanelLocation::Bottom)
        , m_panelSizePolicy(GeneralConfigDefine::PanelSizePolicy::Small)
        , m_panelAutoHide(false)
        , m_panelLock(false)
        , m_taskBarIconsShowedOn(GeneralConfigDefine::TaskBarIconsShowedOn::PanelWhereWindowIsOpen)
        , m_panelType(GeneralConfigDefine::PanelType::Version4)
        , m_showPanelOnAllScreens(true)
        , m_showSystemTrayOnAllPanels(true)
        , m_panelLocationNext(GeneralConfigDefine::PanelLocation::Bottom)
        , m_panelSizePolicyNext(GeneralConfigDefine::PanelSizePolicy::Small)
        , m_panelAutoHideNext(false)
        , m_panelLockNext(false)
{
    qRegisterMetaType<GeneralConfigDefine::MergeIcons>("GeneralConfigDefine::MergeIcons");
    qRegisterMetaType<GeneralConfigDefine::PanelLocation>("GeneralConfigDefine::PanelLocation");
    qRegisterMetaType<GeneralConfigDefine::PanelSizePolicy>("GeneralConfigDefine::PanelSizePolicy");
    qRegisterMetaType<GeneralConfigDefine::TaskBarIconsShowedOn>("GeneralConfigDefine::TaskBarIconsShowedOn");

    m_taskManagerConfig = UkuiQuick::ConfigLoader::getConfig("org.ukui.panel.taskManager");
    m_systemTrayConfig = UkuiQuick::ConfigLoader::getConfig("org.ukui.systemTray");
    m_mergeIcons = m_taskManagerConfig->getValue(QStringLiteral("mergeIcons")).value<GeneralConfigDefine::MergeIcons>();
    m_trayIconsInhibited = m_systemTrayConfig->getValue(QStringLiteral("trayIconsInhibited")).toStringList();

    m_taskBarIconsShowedOn = m_taskManagerConfig->getValue(QStringLiteral("taskBarIconsShowedOn")).value<GeneralConfigDefine::TaskBarIconsShowedOn>();

    connect(m_taskManagerConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onTaskManagerConfigChanged);
    connect(m_systemTrayConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onSystemTrayConfigChanged);

    m_panelConfig = UkuiQuick::ConfigLoader::getConfig("panel", UkuiQuick::ConfigLoader::Local, "ukui-panel");
    m_panelConfig->addGroupInfo(UkuiQuick::Island::viewGroupName(), UkuiQuick::Island::viewGroupKey());
    for(auto config : m_panelConfig->children(UkuiQuick::Island::viewGroupName())) {
        if(config->getValue("id").toString() == "org.ukui.panel") {
            m_mainViewConfig = config;
        } else if(config->getValue("id").toString() == "org.ukui.panelNext") {
            m_mainViewNextConfig = config;
        } else if(config->getValue("id").toString() == "org.ukui.panelTopBar") {
            m_mainViewTopBarConfig = config;
        }
    }

    connect(m_panelConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onPanelConfigChanged);

    m_showPanelOnAllScreens = m_panelConfig->getValue(QStringLiteral("showPanelOnAllScreens")).toBool();
    m_panelType = m_panelConfig->getValue(QStringLiteral("currentPanelVersion")).value<GeneralConfigDefine::PanelType>();
    if (m_mainViewConfig) {
        m_disabledWidgets = m_mainViewConfig->getValue(QStringLiteral("disabledWidgets")).toStringList();
        m_panelSizePolicy = m_mainViewConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
        m_panelAutoHide = m_mainViewConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
        m_panelLock = m_mainViewConfig->getValue(QStringLiteral("lockPanel")).toBool();
        m_panelWidgets = m_mainViewConfig->getValue(QStringLiteral("widgets")).toStringList();
        m_showSystemTrayOnAllPanels = m_mainViewConfig->getValue(QStringLiteral("showSystemTrayOnAllPanels")).toBool();
        connect(m_mainViewConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onPanelViewConfigChanged, Qt::UniqueConnection);
        updatePanelLocation();
    }
    if (m_mainViewNextConfig) {
        m_disabledWidgetsNext = m_mainViewNextConfig->getValue(QStringLiteral("disabledWidgets")).toStringList();
        m_panelSizePolicyNext = m_mainViewNextConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
        m_panelAutoHideNext = m_mainViewNextConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
        m_panelLockNext = m_mainViewNextConfig->getValue(QStringLiteral("lockPanel")).toBool();
        m_panelWidgets = m_mainViewNextConfig->getValue(QStringLiteral("widgets")).toStringList();
        connect(m_mainViewNextConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onPanelViewConfigNextChanged, Qt::UniqueConnection);
        updatePanelLocationNext();
    }

}

void RemoteConfig::setMergeIcons(GeneralConfigDefine::MergeIcons mergeIcons)
{
    if (mergeIcons != m_mergeIcons) {
        m_mergeIcons = mergeIcons;
        m_taskManagerConfig->setValue(QStringLiteral("mergeIcons"), mergeIcons);
        Q_EMIT mergeIconsChanged(m_mergeIcons);
    }
}

void RemoteConfig::setPanelLocation(GeneralConfigDefine::PanelLocation panelLocation)
{
    if (panelLocation != m_panelLocation) {
        m_panelLocation = panelLocation;

        UkuiQuick::Types::Pos pos;
        switch (m_panelLocation) {
            default:
            case GeneralConfigDefine::Bottom:
                pos = UkuiQuick::Types::Bottom;
                break;
            case GeneralConfigDefine::Left:
                pos = UkuiQuick::Types::Left;
                break;
            case GeneralConfigDefine::Top:
                pos = UkuiQuick::Types::Top;
                break;
            case GeneralConfigDefine::Right:
                pos = UkuiQuick::Types::Right;
                break;
        }
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("position"), pos);
        }
        Q_EMIT panelLocationChanged(m_panelLocation);
    }
}

void RemoteConfig::setPanelSizePolicy(GeneralConfigDefine::PanelSizePolicy panelSizePolicy)
{
    if (panelSizePolicy != m_panelSizePolicy) {
        m_panelSizePolicy = panelSizePolicy;
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("panelSizePolicy"), panelSizePolicy);
        }
        Q_EMIT panelSizePolicyChanged(m_panelSizePolicy);
    }
}

void RemoteConfig::setPanelAutoHide(bool panelAutoHide)
{
    if (panelAutoHide != m_panelAutoHide) {
        m_panelAutoHide = panelAutoHide;
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("panelAutoHide"), panelAutoHide);
        }
        Q_EMIT panelAutoHideChanged(m_panelAutoHide);
    }
}

void RemoteConfig::setPanelLock(bool panelLock)
{
    if (panelLock != m_panelLock) {
        m_panelLock = panelLock;
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("lockPanel"), panelLock);
        }
        Q_EMIT panelLockChanged(m_panelLock);
    }
}

void RemoteConfig::setPanelWidgets(QStringList panelWidgets)
{
    if (panelWidgets != m_panelWidgets) {
        m_panelWidgets = panelWidgets;
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("widgets"), m_panelWidgets);
        }
        Q_EMIT panelWidgetsChanged(m_panelWidgets);
    }
}

void RemoteConfig::setTrayIconsInhibited(QStringList trayIconsInhibited)
{
    if (trayIconsInhibited != m_trayIconsInhibited) {
        m_trayIconsInhibited = trayIconsInhibited;
        m_systemTrayConfig->setValue(QStringLiteral("trayIconsInhibited"), trayIconsInhibited);
        Q_EMIT trayIconsInhibitedChanged(m_trayIconsInhibited);
    }
}

void RemoteConfig::onPanelViewConfigChanged(const QString &key)
{
    if(!m_mainViewConfig) {
        return;
    }
    if (key == QStringLiteral("disabledWidgets")) {
        m_disabledWidgets = m_mainViewConfig->getValue(key).toStringList();
        Q_EMIT disabledWidgetsChanged(m_disabledWidgets);
    } else if(key == "panelSizePolicy") {
        auto value = m_mainViewConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
        if(value != m_panelSizePolicy) {
            m_panelSizePolicy = value;
            Q_EMIT panelSizePolicyChanged(m_panelSizePolicy);
        }
    } else if (key == "panelAutoHide") {
        bool value = m_mainViewConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
        if(value != m_panelAutoHide) {
            m_panelAutoHide = value;
            Q_EMIT panelAutoHideChanged(m_panelAutoHide);
        }
    } else if (key == "lockPanel") {
        bool value = m_mainViewConfig->getValue(QStringLiteral("lockPanel")).toBool();
        if(value != m_panelLock) {
            m_panelLock = value;
            Q_EMIT panelLockChanged(m_panelLock);
        }
    } else if (key == "widgets") {
        QStringList value = m_mainViewConfig->getValue(QStringLiteral("widgets")).toStringList();
        if(value != m_panelWidgets) {
            m_panelWidgets = value;
            Q_EMIT panelWidgetsChanged(m_panelWidgets);
        }
    } else if (key == "position") {
        updatePanelLocation();
        Q_EMIT panelLocationChanged(m_panelLocation);
    } else if (key == "showSystemTrayOnAllPanels") {
        bool value = m_mainViewConfig->getValue(QStringLiteral("showSystemTrayOnAllPanels")).toBool();
        if(value != m_showSystemTrayOnAllPanels) {
            m_showSystemTrayOnAllPanels = value;
            Q_EMIT showSystemTrayOnAllPanelsChanged(m_showSystemTrayOnAllPanels);
        }
    }
}

void RemoteConfig::onPanelConfigChanged(const QString &key)
{
    if (key == "currentPanelVersion") {
        auto value = m_panelConfig->getValue(QStringLiteral("currentPanelVersion")).value<GeneralConfigDefine::PanelType>();
        if(value != m_panelType) {
            m_panelType = value;
            Q_EMIT panelTypeChanged(m_panelType);
        }
    } else if (key == "showPanelOnAllScreens") {
        bool value = m_panelConfig->getValue(QStringLiteral("showPanelOnAllScreens")).toBool();
        if(value != m_showPanelOnAllScreens) {
            m_showPanelOnAllScreens = value;
            Q_EMIT showPanelOnAllScreensChanged(m_showPanelOnAllScreens);
        }
    }
}

void RemoteConfig::onTaskManagerConfigChanged(const QString &key)
{
    if(key == "mergeIcons") {
        auto value = m_taskManagerConfig->getValue(QStringLiteral("mergeIcons")).value<GeneralConfigDefine::MergeIcons>();
        if(value != m_mergeIcons) {
            m_mergeIcons = value;
            Q_EMIT mergeIconsChanged(m_mergeIcons);
        }
    } else if (key == "taskBarIconsShowedOn") {
        auto value = m_taskManagerConfig->getValue(QStringLiteral("taskBarIconsShowedOn")).value<GeneralConfigDefine::TaskBarIconsShowedOn>();
        if(value != m_taskBarIconsShowedOn) {
            m_taskBarIconsShowedOn = value;
            Q_EMIT taskBarIconsShowedOnChanged(m_taskBarIconsShowedOn);
        }
    }
}

void RemoteConfig::onSystemTrayConfigChanged(const QString &key)
{
    if(key == "trayIconsInhibited") {
        auto value = m_systemTrayConfig->getValue(QStringLiteral("trayIconsInhibited")).toStringList();
        if(value != m_trayIconsInhibited) {
            m_trayIconsInhibited = value;
            Q_EMIT trayIconsInhibitedChanged(m_trayIconsInhibited);
        }
    }
}

void RemoteConfig::updatePanelLocation()
{
    if(!m_mainViewConfig) {
        return;
    }
    auto pos = m_mainViewConfig->getValue(QStringLiteral("position")).value<UkuiQuick::Types::Pos>();
    switch (pos) {
        case UkuiQuick::Types::Left:
            m_panelLocation = GeneralConfigDefine::PanelLocation::Left;
            break;
        case UkuiQuick::Types::Top:
            m_panelLocation = GeneralConfigDefine::PanelLocation::Top;
            break;
        case UkuiQuick::Types::Right:
            m_panelLocation = GeneralConfigDefine::PanelLocation::Right;
            break;
        default:
        case UkuiQuick::Types::Bottom:
            m_panelLocation = GeneralConfigDefine::PanelLocation::Bottom;
            break;
    }
}

QStringList RemoteConfig::disabledWidgets() const
{
    return m_disabledWidgets;
}

void RemoteConfig::disableWidget(const QString &id, bool disable)
{
    if (!m_mainViewConfig) {
        return;
    }

    if (disable) {
        if (m_disabledWidgets.contains(id)) {
            return;
        }
        m_disabledWidgets.append(id);

    } else if (m_disabledWidgets.removeAll(id) == 0) {
        return;
    }

    m_mainViewConfig->setValue(QStringLiteral("disabledWidgets"), m_disabledWidgets);
}

void RemoteConfig::setTaskBarIconsShowedOn(GeneralConfigDefine::TaskBarIconsShowedOn taskBarIconsShowedOn)
{
    if (taskBarIconsShowedOn != m_taskBarIconsShowedOn) {
        m_taskBarIconsShowedOn = taskBarIconsShowedOn;
        if(m_taskManagerConfig) {
            m_taskManagerConfig->setValue(QStringLiteral("taskBarIconsShowedOn"), taskBarIconsShowedOn);
        }
        Q_EMIT taskBarIconsShowedOnChanged(m_taskBarIconsShowedOn);
    }
}

void RemoteConfig::setPanelType(GeneralConfigDefine::PanelType panelType)
{
    if (panelType != m_panelType) {
        m_panelType = panelType;
        Q_EMIT panelTypeChanged(m_panelType);
        if(m_panelConfig) {
            m_panelConfig->setValue(QStringLiteral("currentPanelVersion"), m_panelType);
        }
    }
}

void RemoteConfig::setShowPanelOnAllScreens(bool showPanelOnAllScreens)
{
    if (showPanelOnAllScreens != m_showPanelOnAllScreens) {
        m_showPanelOnAllScreens = showPanelOnAllScreens;
        if(m_panelConfig) {
            m_panelConfig->setValue(QStringLiteral("showPanelOnAllScreens"), showPanelOnAllScreens);
        }
        Q_EMIT showPanelOnAllScreensChanged(m_showPanelOnAllScreens);
    }
}

void RemoteConfig::setShowSystemTrayOnAllPanels(bool showSystemTrayOnAllPanels)
{
    if (showSystemTrayOnAllPanels != m_showSystemTrayOnAllPanels) {
        m_showSystemTrayOnAllPanels = showSystemTrayOnAllPanels;
        if(m_mainViewConfig) {
            m_mainViewConfig->setValue(QStringLiteral("showSystemTrayOnAllPanels"), showSystemTrayOnAllPanels);
        }
        Q_EMIT showSystemTrayOnAllPanelsChanged(m_showSystemTrayOnAllPanels);
    }
}

void RemoteConfig::setPanelLocationNext(GeneralConfigDefine::PanelLocation panelLocation)
{
    if (panelLocation != m_panelLocationNext) {
        m_panelLocationNext = panelLocation;

        UkuiQuick::Types::Pos pos;
        switch (m_panelLocationNext) {
            default:
            case GeneralConfigDefine::Bottom:
                pos = UkuiQuick::Types::Bottom;
                break;
            case GeneralConfigDefine::Left:
                pos = UkuiQuick::Types::Left;
                break;
            case GeneralConfigDefine::Top:
                pos = UkuiQuick::Types::Top;
                break;
            case GeneralConfigDefine::Right:
                pos = UkuiQuick::Types::Right;
                break;
        }
        if(m_mainViewNextConfig) {
            m_mainViewNextConfig->setValue(QStringLiteral("position"), pos);
        }
        Q_EMIT panelLocationNextChanged(m_panelLocationNext);
    }
}

void RemoteConfig::setPanelSizePolicyNext(GeneralConfigDefine::PanelSizePolicy panelSizePolicy)
{
    if (panelSizePolicy != m_panelSizePolicyNext) {
        m_panelSizePolicyNext = panelSizePolicy;
        if(m_mainViewNextConfig) {
            m_mainViewNextConfig->setValue(QStringLiteral("panelSizePolicy"), panelSizePolicy);
        }
        Q_EMIT panelSizePolicyNextChanged(m_panelSizePolicyNext);
    }
}

void RemoteConfig::setPanelAutoHideNext(bool panelAutoHide)
{
    if (panelAutoHide != m_panelAutoHideNext) {
        m_panelAutoHideNext = panelAutoHide;
        if(m_mainViewNextConfig) {
            m_mainViewNextConfig->setValue(QStringLiteral("panelAutoHide"), panelAutoHide);
        }
        Q_EMIT panelAutoHideNextChanged(m_panelAutoHideNext);
    }
}

void RemoteConfig::setPanelLockNext(bool panelLock)
{
    if (panelLock != m_panelLockNext) {
        m_panelLockNext = panelLock;
        if(m_mainViewNextConfig) {
            m_mainViewNextConfig->setValue(QStringLiteral("lockPanel"), panelLock);
        }
        Q_EMIT panelLockNextChanged(m_panelLockNext);
    }
}

void RemoteConfig::onPanelViewConfigNextChanged(const QString &key)
{
    if(!m_mainViewNextConfig) {
        return;
    }
    if (key == QStringLiteral("disabledWidgets")) {
        m_disabledWidgetsNext = m_mainViewNextConfig->getValue(key).toStringList();
        Q_EMIT disabledWidgetsNextChanged(m_disabledWidgetsNext);
    } else if(key == "panelSizePolicy") {
        auto value = m_mainViewNextConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
        if(value != m_panelSizePolicyNext) {
            m_panelSizePolicyNext = value;
            Q_EMIT panelSizePolicyNextChanged(m_panelSizePolicyNext);
        }
    } else if (key == "panelAutoHide") {
        bool value = m_mainViewNextConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
        if(value != m_panelAutoHideNext) {
            m_panelAutoHideNext = value;
            Q_EMIT panelAutoHideNextChanged(m_panelAutoHideNext);
        }
    } else if (key == "lockPanel") {
        bool value = m_mainViewNextConfig->getValue(QStringLiteral("lockPanel")).toBool();
        if(value != m_panelLockNext) {
            m_panelLockNext = value;
            Q_EMIT panelLockNextChanged(m_panelLockNext);
        }
    } else if (key == "widgets") {
        QStringList value = m_mainViewNextConfig->getValue(QStringLiteral("widgets")).toStringList();
        if(value != m_panelWidgetsNext) {
            m_panelWidgetsNext = value;
            Q_EMIT panelWidgetsNextChanged(m_panelWidgetsNext);
        }
    } else if (key == "position") {
        updatePanelLocationNext();
        Q_EMIT panelLocationNextChanged(m_panelLocationNext);
    }
}

void RemoteConfig::updatePanelLocationNext()
{
    if(!m_mainViewNextConfig) {
        return;
    }
    auto pos = m_mainViewNextConfig->getValue(QStringLiteral("position")).value<UkuiQuick::Types::Pos>();
    switch (pos) {
        case UkuiQuick::Types::Left:
            m_panelLocationNext = GeneralConfigDefine::PanelLocation::Left;
            break;
        case UkuiQuick::Types::Top:
            m_panelLocationNext = GeneralConfigDefine::PanelLocation::Top;
            break;
        case UkuiQuick::Types::Right:
            m_panelLocationNext = GeneralConfigDefine::PanelLocation::Right;
            break;
        default:
        case UkuiQuick::Types::Bottom:
            m_panelLocationNext = GeneralConfigDefine::PanelLocation::Bottom;
            break;
    }
}

void RemoteConfig::disableWidgetNext(const QString &id, bool disable)
{
    if (!m_mainViewNextConfig) {
        return;
    }

    if (disable) {
        if (m_disabledWidgetsNext.contains(id)) {
            return;
        }
        m_disabledWidgetsNext.append(id);

    } else if (m_disabledWidgetsNext.removeAll(id) == 0) {
        return;
    }

    m_mainViewNextConfig->setValue(QStringLiteral("disabledWidgets"), m_disabledWidgetsNext);
    if(!m_mainViewTopBarConfig) {
        return;
    }
    m_mainViewTopBarConfig->setValue(QStringLiteral("disabledWidgets"), m_disabledWidgetsNext);
}

void RemoteConfig::setPanelWidgetsNext(QStringList panelWidgets)
{
    if (panelWidgets != m_panelWidgetsNext) {
        m_panelWidgetsNext = panelWidgets;
        if(m_mainViewNextConfig) {
            m_mainViewNextConfig->setValue(QStringLiteral("widgets"), m_panelWidgetsNext);
        }
        Q_EMIT panelWidgetsNextChanged(m_panelWidgetsNext);
    }
}

void RemoteConfig::updateMainViewConfig()
{
    for(auto config : m_panelConfig->children(UkuiQuick::Island::viewGroupName())) {
        if(config->getValue("id").toString() == "org.ukui.panel") {
            if(!m_mainViewConfig) {
                m_mainViewConfig = config;
                m_disabledWidgets = m_mainViewConfig->getValue(QStringLiteral("disabledWidgets")).toStringList();
                Q_EMIT disabledWidgetsChanged(m_disabledWidgets);
                m_panelSizePolicy = m_mainViewConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
                Q_EMIT panelSizePolicyChanged(m_panelSizePolicy);
                m_panelAutoHide = m_mainViewConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
                Q_EMIT panelAutoHideChanged(m_panelAutoHide);
                m_panelLock = m_mainViewConfig->getValue(QStringLiteral("lockPanel")).toBool();
                Q_EMIT panelLockChanged(m_panelLock);
                m_panelWidgets = m_mainViewConfig->getValue(QStringLiteral("widgets")).toStringList();
                Q_EMIT panelWidgetsChanged(m_panelWidgets);
                m_showSystemTrayOnAllPanels = m_mainViewConfig->getValue(QStringLiteral("showSystemTrayOnAllPanels")).toBool();
                Q_EMIT showSystemTrayOnAllPanelsChanged(m_showSystemTrayOnAllPanels);
                connect(m_mainViewConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onPanelViewConfigChanged, Qt::UniqueConnection);
                updatePanelLocation();
                Q_EMIT panelLocationChanged(m_panelLocation);
            }
        } else if(config->getValue("id").toString() == "org.ukui.panelNext") {
            if(!m_mainViewNextConfig) {
                m_mainViewNextConfig = config;
                m_disabledWidgetsNext = m_mainViewNextConfig->getValue(QStringLiteral("disabledWidgets")).toStringList();
                Q_EMIT disabledWidgetsNextChanged(m_disabledWidgetsNext);
                m_panelSizePolicyNext = m_mainViewNextConfig->getValue(QStringLiteral("panelSizePolicy")).value<GeneralConfigDefine::PanelSizePolicy>();
                Q_EMIT panelSizePolicyNextChanged(m_panelSizePolicyNext);
                m_panelAutoHideNext = m_mainViewNextConfig->getValue(QStringLiteral("panelAutoHide")).toBool();
                Q_EMIT panelAutoHideNextChanged(m_panelAutoHideNext);
                m_panelLockNext = m_mainViewNextConfig->getValue(QStringLiteral("lockPanel")).toBool();
                Q_EMIT panelLockNextChanged(m_panelLockNext);
                m_panelWidgets = m_mainViewNextConfig->getValue(QStringLiteral("widgets")).toStringList();
                Q_EMIT panelWidgetsNextChanged(m_panelWidgetsNext);
                connect(m_mainViewNextConfig, &UkuiQuick::Config::configChanged, this, &RemoteConfig::onPanelViewConfigNextChanged, Qt::UniqueConnection);
                updatePanelLocationNext();
                Q_EMIT panelLocationNextChanged(m_panelLocationNext);
            }
        } else if(config->getValue("id").toString() == "org.ukui.panelTopBar") {
            if(!m_mainViewTopBarConfig) {
                m_mainViewTopBarConfig = config;
            }
        }
    }
}
