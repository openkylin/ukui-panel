<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>PanelUkccPlugin</name>
    <message>
        <source>Panel</source>
        <translation>ལས་འགན་གྱི་ལན་ཀན་</translation>
    </message>
    <message>
        <source>Merge icons on the taskbar</source>
        <translation>ཟླ་སྒྲིལ་གཏོང་བའི་ལས་འགན་གྱི་རི་མོའི་སྟེང་གི་དཔེ་རིས།</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>ཐོག་མཐའ་བར་གསུམ་</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>གཏན་ནས་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Panel location</source>
        <translation>ལས་འགན་བརྙན་ཤེལ་ཐོག་གི་གནས་སུ་ཡོད།</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>མཐིལ།</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>གཡོན་ལོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>རྩེ་མོ།</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>གཡས་ལོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <source>Panel size</source>
        <translation>ལས་འགན་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>ཆུང་བ།</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ཀྲུང་གོའི་གུང་ཀྲུང་</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>རང་ཉིད་མཚན་ཉིད་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Panel auto hide</source>
        <translation>རང་འགུལ་གྱིས་ལས་འགན་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Panel lock</source>
        <translation>ལས་འགན་གྱི་ལན་ཀན་བཀག་སྡོམ་བྱེད་</translation>
    </message>
    <message>
        <source>Widgets always showed on panel</source>
        <translation>ཐོག་མཐའ་བར་གསུམ་དུ་ལས་འགན་གྱི་རི་མོའི་སྟེང་དུ་མངོན་པ་རེད།</translation>
    </message>
    <message>
        <source>Task view</source>
        <translation>ལས་འགན་མང་པོས་རི་མོ་ལ་བལྟ་བ</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>སྐད་སྒྲ།</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <source>Power</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>སོ་སྔོན་པོ།</translation>
    </message>
    <message>
        <source>Icons showed on system tray</source>
        <translation>སྡེར་མའི་མ་ལག་གི་དཔེ་རིས་མཚོན་པ།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>When existing multiple panels, show window icons on</source>
        <translation>ལས་འགན་གྱི་ལན་ཀན་མང་པོའི་དུས་སུ་ལས་འགན་གྱི་རི་མོའི་སྟེང་དུ་མངོན་པ་རེད།</translation>
    </message>
    <message>
        <source>All panels</source>
        <translation>ལས་འགན་གྱི་ལན་ཀན་ཡོད་ཚད</translation>
    </message>
    <message>
        <source>Primary screen panel and panel where window is open</source>
        <translation>བརྙན་ཤེལ་གཙོ་བོའི་ངོས་དང་སྒེའུ་ཁུང་གི་ལས་འགན་གྱི་སྒྲོམ་བུ་ཕྱེ་ཡོད།</translation>
    </message>
    <message>
        <source>Panel where window is open</source>
        <translation>སྒེའུ་ཁུང་ཕྱེས་པའི་ལས་འགན་གྱི་ལན་ཀན་ཁོ་ན་མངོན་པ་རེད།</translation>
    </message>
    <message>
        <source>Show panel on all screens</source>
        <translation>བརྡ་སྟོན་ཡོ་བྱད་ཚང་མའི་སྟེང་ལས་འགན་གྱི་ལན་ཀན་ཡོད།</translation>
        <extra-contents_path>/Panel/Show panel on all screens</extra-contents_path>
    </message>
    <message>
        <source>Show system tray area on all panels </source>
        <translation>ལས་འགན་ཡོད་ཚད་ཀྱི་སྟེང་དུ་སྡེར་མ་བཀོལ་སྤྱོད་བྱེད་པའིས་ཁོངས་མངོན་པར་བྱ་དགོས། </translation>
    </message>
    <message>
        <source>Panel Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>threeIsland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>VPN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fcitx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Virtual Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Never Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Always Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show When Keyboard Is Connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>Task view</source>
        <translation type="obsolete">多任务视图</translation>
    </message>
</context>
</TS>
