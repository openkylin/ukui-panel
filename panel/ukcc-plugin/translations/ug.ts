<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>PanelUkccPlugin</name>
    <message>
        <source>Panel</source>
        <translation>ۋەزىپە ئىستونى</translation>
        <extra-contents_path>/Panel/Panel</extra-contents_path>
    </message>
    <message>
        <source>Merge icons on the taskbar</source>
        <translation>ۋەزىپە ئىستونىدىكى سىنبەلگىلەرنى بىرلەشتۈرۈش</translation>
        <extra-contents_path>/Panel/Merge icons on the taskbar</extra-contents_path>
    </message>
    <message>
        <source>Always</source>
        <translation>باشتىن ئاخىر</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>ھەرگىز</translation>
    </message>
    <message>
        <source>Panel location</source>
        <translation>ۋەزىپە ستونى ئېكراندىكى ئورنى</translation>
        <extra-contents_path>/Panel/Panel location</extra-contents_path>
    </message>
    <message>
        <source>Bottom</source>
        <translation>ئاستى</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>سول</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>ئۈستى</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>ئوڭ</translation>
    </message>
    <message>
        <source>Panel size</source>
        <translation>ۋەزىپە ستونى چوڭ-كىچىكلىكى</translation>
        <extra-contents_path>/Panel/Panel size</extra-contents_path>
    </message>
    <message>
        <source>Small</source>
        <translation>كىچىك</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ئوتتۇراھال</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>چوڭ</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ئىختىيارى</translation>
    </message>
    <message>
        <source>Panel auto hide</source>
        <translation>ئاپتوماتىك يوشۇرۇن ۋەزىپە ستونى</translation>
        <extra-contents_path>/Panel/Panel auto hide</extra-contents_path>
    </message>
    <message>
        <source>Panel lock</source>
        <translation>قۇلۇپلاش ۋەزىپە ستونى</translation>
        <extra-contents_path>/Panel/Panel lock</extra-contents_path>
    </message>
    <message>
        <source>Widgets always showed on panel</source>
        <translation>باشتىن-ئاخىر كۆرسىتىش ۋەزىپە ستونى ئۈستىدىكى سىنبەلگە</translation>
        <extra-contents_path>/Panel/Widgets always showed on panel</extra-contents_path>
    </message>
    <message>
        <source>Task view</source>
        <translation>كۆپ ۋەزىپىلىك كۆرۈنۈش</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>ئاۋاز</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>تور</translation>
    </message>
    <message>
        <source>Power</source>
        <translation>توك مەنبەسىنى باشقۇرۇش</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>كۆك چىش</translation>
    </message>
    <message>
        <source>Icons showed on system tray</source>
        <translation>پەتنۇس كۆرسىتىش سىستېمىسى سىنبەلگە</translation>
        <extra-contents_path>/Panel/Icons showed on system tray</extra-contents_path>
    </message>
    <message>
        <source>Panel Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>threeIsland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show panel on all screens</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Panel/Show panel on all screens</extra-contents_path>
    </message>
    <message>
        <source>Show system tray area on all panels </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>When existing multiple panels, show window icons on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Primary screen panel and panel where window is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel where window is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>VPN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fcitx</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Virtual Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Never Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Always Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show When Keyboard Is Connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>Task view</source>
        <translation type="obsolete">多任务视图</translation>
    </message>
</context>
</TS>
