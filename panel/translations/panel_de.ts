<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>Desktop anzeigen</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>Panel-Größe</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>Groß</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>Klein</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Gewohnheit</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Unteres</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>Bedienfeld sperren</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Desktop anzeigen</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished">Bedienfeld sperren</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished">Panel-Größe</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">Groß</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">Mittel</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">Klein</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">Gewohnheit</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">Desktop anzeigen</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
