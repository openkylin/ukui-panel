<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>UkuiPanel::Panel</name>
    <message>
        <source>Show Desktop</source>
        <translation>شىرە  بەتىن كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation>ساامالىق  كۉزۅتكۉچ</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation>اپتوماتتىك جاشىرۇۇ</translation>
    </message>
    <message>
        <source>Panel Setting</source>
        <translation>تاقتا  تەڭشەگى</translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation>مىلدەت قۇرۇنۇن  چوڭ-كىچىكتىگى</translation>
    </message>
    <message>
        <source>Large</source>
        <translation>چوڭ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>ورتوسۇ</translation>
    </message>
    <message>
        <source>Small</source>
        <translation>كىچىك</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ئۆرپ-ادات</translation>
    </message>
    <message>
        <source>Panel Position</source>
        <translation>تاقتا  وردۇ</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>ەڭ ۉستۉ</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>استىنقى</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>سول</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>توغرىسى</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation>مىلدەت  ىستونۇن قۇلۇپتوو</translation>
    </message>
    <message>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to New Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelNext</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">شىرە  بەتىن كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">ساامالىق  كۉزۅتكۉچ</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="unfinished">اپتوماتتىك جاشىرۇۇ</translation>
    </message>
    <message>
        <source>Lock Panel</source>
        <translation type="unfinished">مىلدەت  ىستونۇن قۇلۇپتوو</translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Panel Size</source>
        <translation type="unfinished">مىلدەت قۇرۇنۇن  چوڭ-كىچىكتىگى</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="unfinished">چوڭ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="unfinished">ورتوسۇ</translation>
    </message>
    <message>
        <source>Small</source>
        <translation type="unfinished">كىچىك</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished">ئۆرپ-ادات</translation>
    </message>
</context>
<context>
    <name>UkuiPanel::PanelTopBar</name>
    <message>
        <source>Panel Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Desktop</source>
        <translation type="unfinished">شىرە  بەتىن كۅرسۅتۉۉ</translation>
    </message>
    <message>
        <source>System Monitor</source>
        <translation type="unfinished">ساامالىق  كۉزۅتكۉچ</translation>
    </message>
    <message>
        <source>Data Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Islands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings Island Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch to Classical Panel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
